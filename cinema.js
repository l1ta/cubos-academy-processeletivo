const pessoas = [
	{
	    "temIngresso": true,
	    "idade": 23,
	    "temCarteirinha": true,
	    "censura": 16,
	    "estaComPais": false
	},
	{
		"temIngresso": true,
		"idade": 23,
		"temCarteirinha": false,
		"censura": 16,
		"estaComPais": true
	},
	{
		"temIngresso": false,
		"idade": 23,
		"temCarteirinha": true,
		"censura": 18,
		"estaComPais": true
	},
	{
		"temIngresso": true,
		"idade": 15,
		"temCarteirinha": false,
		"censura": 16,
		"estaComPais": true
	},
	{
		"temIngresso": true,
		"idade": 15,
		"temCarteirinha": false,
		"censura": 16,
		"estaComPais": false
	},
	{
	    "temIngresso": true,
	    "idade": 23,
	    "temCarteirinha": true,
	    "censura": 16,
	    "estaComPais": true
	}
];




function solucao(obj) {
	if (obj.temIngresso && (obj.idade >= obj.censura || obj.estaComPais))
	{
		if(obj.idade < 18 || obj.temCarteirinha)
		{
			console.log("MEIA");
		} else {
			console.log("INTEIRA");
		}
	} else
	{
		console.log("ACESSO NEGADO");
	}
}



pessoas.map(elem=> {
	console.log(elem);
	console.log(solucao(elem));
	console.log("\n\n");
})