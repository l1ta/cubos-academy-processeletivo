const pessoas = [
	{
	    "tempo": 5,
	    "distancia": 16,
	},
	{
		"tempo": 4,
		"distancia": 16,
	},
	{
		"tempo": 61,
		"distancia": 18,
	},
	{
		"tempo": 120,
		"distancia": 150,
	},
	{
		"tempo": 15,
		"distancia": 90,
	},
	{
	    "tempo": 2,
	    "distancia": 1,
	}
];
        // Percorre cada elemento do Array.
pessoas.map(item=> 
{
	// console.log(item.tempo);
	solucao(item.tempo,item.distancia);
	console.log("\n\n");
}
);


function solucao(tempo, distancia){
	let total;
	if(tempo < 5){
		total = 6;
	} else if (tempo >= 5 && tempo <= 60) {
		total = (tempo * 1) + (0.50 * distancia);
	} else {
		if(distancia < 100 ){
			total = 2 * distancia;
		} else {
			total = 1.50 * distancia;
		}
	}
	console.log(total * 100);
}