/*
	Desenvolver software do cinema, que indica SE uma pessoa pode ter acesso ao filme.
	Verifica SE a pessoa tem direito a meia entrada || SE o ingresso deve ser inteira.
	-
    Ter ingresso.
    Ter idade = || > a censura || estar acompanhada dos pais.

    ...é necessário verificar se ela tem direito a meia.
    Condições abaixo:
	
	-
    Ter carteirinha de estudante.
    Ter menos de 18 anos.

	A ENTRADA será sempre um objeto, com os dados necessários para a análise, no seguinte formato:

{
    temIngresso: true,
    idade: 23,
    temCarteirinha: true,
    censura: 16,
    estaComPais: false
}

	A SAÍDA deverá ser sempre uma das três opções abaixo:
	-
    ACESSO NEGADO caso a pessoa não possa ter acesso ao filme.
    INTEIRA caso a pessoa tenha acesso ao filme mediante apresentação de ingresso de inteira.
    MEIA caso a pessoa tenha acesso ao filme mediante apresentação de ingresso de meia entrada.

*/

function solucao(obj) {
	for(){
		
	}
}