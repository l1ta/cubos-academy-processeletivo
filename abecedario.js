
/*  Programa que calcule quantas crianças perderam na rodada.
	A entrada terá sempre duas variáveis.
	Imprima na tela apenas um número inteiro contendo a 
quantidade de crianças que PERDERAM nessa rodada.
*/

const categoria = [
	{
    "letra": "m",
    "palavras": [
		"mamao",
		"maca",
		"melao",
		"melancia",
		"laranja"
		]
  	},
	{
    "letra": "m",
    "palavras": [
		"mamao",
		"banena",
		"melao",
		"melancia",
		"laranja"
		]
  	},
];


categoria.map(item=> {
	// console.log(item.letra, item.palavras);
	solucao(item.letra, item.palavras);
	console.log("\n\n");
})



function solucao(letra, palavras) {
	let totalPerderam = 0;
	for(let item of palavras)
	{
		if(item[0] != letra) {
			totalPerderam++;
		}
	}
	console.log(totalPerderam);
}
