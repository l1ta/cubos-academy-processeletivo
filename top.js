const produtos = [[
	  {
	    "nome": "Camiseta",
	    "preco": 7000
	  },
	  {
	    "nome": "Tenis",
	    "preco": 8000
	  },
	  {
	    "nome": "Relogio",
	    "preco": 15000
	  }
	],
	[
  {
    "nome": "Mack",
    "preco": 16155
  },
  {
    "nome": "Bates",
    "preco": 5801
  },
  {
    "nome": "Cote",
    "preco": 15934
  },
  {
    "nome": "Norris",
    "preco": 9792
  },
  {
    "nome": "Dudley",
    "preco": 2904
  },
  {
    "nome": "Wagner",
    "preco": 15631
  },
  {
    "nome": "Knox",
    "preco": 2920
  },
  {
    "nome": "Santiago",
    "preco": 13910
  },
  {
    "nome": "Wiggins",
    "preco": 5888
  },
  {
    "nome": "Witt",
    "preco": 13024
  },
  {
    "nome": "Cross",
    "preco": 10026
  },
  {
    "nome": "Benson",
    "preco": 17395
  }
]
];

produtos.map(elem=> {
	// console.log(elem);
	solucao(elem);
	console.log("\n");
})


function solucao(produtos) {
	let tops = produtos.filter((item)=> {
		return item.preco > 10000;
	});
	let totalTop = 0;
	for(let i = 0; i < tops.length; i++) {
		totalTop = totalTop + tops[i].preco;
	}
	let total = 0;
	for(let i = 0; i < produtos.length; i++) {
		total = total + produtos[i].preco;
	}
	console.log({
		totalTop: totalTop,
		percentual: (totalTop*100/total)/100
	});
}
