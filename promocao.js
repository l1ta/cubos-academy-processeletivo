const categoria = [
	[100, 500, 100, 200, 50],
	[100, 100, 100, 50],
	[100, 200, 100, 100, 50],
	[100, 200, 50, 100, 50],
	[100, 10, 50, 100, 50],
	[50, 100, 100, 100, 200],
];

categoria.map(item=> {
	// console.log(item);
	solucao(item);
	console.log("\n");
});

function solucao(precos) {
	let total = 0;
	function getMenorIndice(precos) {
		let menor = precos[0];
		let indeceMenor = 0;
		for (let i = 0; i <= precos.length; i++) {
			if(precos[i] < menor) {
				menor = precos[i];
				indeceMenor = i;
			}
		}
		return indeceMenor;
	}
	if(precos.length >= 5) {
		precos.splice(getMenorIndice(precos), 1);
	}
	total = precos.reduce(function (x, y) {
        return x + y;
    }, 0);
	console.log(total);
}
