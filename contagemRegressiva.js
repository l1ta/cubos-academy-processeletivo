
/*
    Programa que, dado um número a partir do qual deve-se começar a contagem regressiva, 
IMPRIMA na tela a contagem regressiva desse número até 0. Ao final da contagem, 
IMPRIMA a mensagem KABUM.
    Importante: cada número deve ser impresso em uma nova linha,
 assim como a mensagem KABUM ao final.
*/


function solucao(numero) {
    while (numero >= 0) {
        // console.log("\n\n ANTES ", numero);
        console.log(numero);
        if(numero == 0){
        console.log('KABUM');
        }
        // numero = numero - 1;
        numero --;
        // console.log("DEPOIS ", numero);
    }
    return;
}

solucao(10);

/*
    function solucao(numero) {
    let count;
    count = count - 1;
    return;
  }
console.log('KABUM');
*/